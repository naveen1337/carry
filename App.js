/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useEffect} from 'react';
import {Provider} from 'react-redux';
import {StyleSheet, StatusBar} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import Geocoder from 'react-native-geocoding';
import SplashScreen from 'react-native-splash-screen';
import DrawerNavigation from './app/navigations/DrawerNavigation';
import AppColors from './app/utils/appColors';
import store from './app/store';
import {KEYS} from './app/utils/constants';
import {getNetworkState} from './app/utils/utilService';

const App = () => {
  /**
   * Method to handle push notifications
   * // handle navigation to specific pages using conditions
   */
  const firebaseListeners = () => {
    firebase.messaging().onMessage((message) => {
      firebase.notifications().displayNotification(message);
    });
    firebase.notifications().onNotification((notification) => {
      firebase.notifications().displayNotification(notification);
    });
    firebase.notifications().onNotificationOpened((notificationOpen) => {
      firebase.notifications().displayNotification(notificationOpen);
    });
  };

  useEffect(() => {
    (async () => {
      await getNetworkState();
      // firebaseListeners();
      // Geocoder.init(KEYS.MAP_KEY, {
      //   language: 'en',
      // });
      // SplashScreen.hide();
    })();
  }, []);

  return (
    <Provider store={store}>
      <NavigationContainer>
        <StatusBar
          barStyle="light-content"
          backgroundColor={AppColors.primaryColor}
        />
        <DrawerNavigation />
      </NavigationContainer>
    </Provider>
  );
};

const styles = StyleSheet.create({});

export default App;
