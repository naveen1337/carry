import React, {useState, useEffect} from 'react';
import {View, StyleSheet, Platform} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {handleError, handleLoading} from '../actions/appActions';
import {fetchNearByHotels} from '../actions/homeAction';
import SampleButton from '../components/SampleButton';
import SampleLoadingComponent from '../components/SampleLoadingComponent';
import AppColors from '../utils/appColors';
import {
  getLocationCoordinates,
  requestLocationPermission,
} from '../utils/utilService';

const HomeScreen = () => {
  const dispatch = useDispatch();
  const showLoading = useSelector((state) => state.app.isLoading);
  const showError = useSelector((state) => state.app.isError);
  const locationState = useSelector((state) => state.app.location);
  const nearByHotelsState = useSelector((state) => state.home.nearbyHotels);

  /**
   * Method to handle sample action
   */
  const handleSampleAction = () => {
    console.log('Sample Action Logic handling');
  };

  const fetchInitialCalls = async () => {
    try {
      // if (Platform.OS == 'android') {
      //   await requestLocationPermission();
      // }
      // await getLocationCoordinates();
      dispatch(fetchNearByHotels(locationState));
    } catch (err) {
      dispatch(handleError(true));
    }
  };

  /**
   * Use Effect method to handle component did mount functinalities
   */
  useEffect(() => {
    (async () => {
      await fetchInitialCalls();
    })();
  }, []);

  return (
    <View style={styles.container}>
      <SampleButton
        buttonText="sample button"
        handleButtonClick={handleSampleAction}
      />
      {showLoading && <SampleLoadingComponent />}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default HomeScreen;
