import I18n from 'i18n-js';
import * as RNLocalize from 'react-native-localize';
//import all locales
import en from './en.json';

const currentLocale = RNLocalize.getLocales();
I18n.locale = currentLocale[0].languageCode;

//Should fallback to english if user locale doesnt exist
I18n.fallbacks = true;

I18n.translations = {
  en,
};

export function strings(name, params = {}) {
  return I18n.t(name, params);
}

export default I18n;
