import {fetchNearByHotelsRemote} from '../utils/sharedService';
import {SAVE_NEARBY_HOTELS} from './actionTypes';
import {handleError, handleLoading} from './appActions';

/**
 * Ation to save nearby hotels
 * @param {*} payload
 */
export const saveNearbyHotels = (payload) => ({
  type: SAVE_NEARBY_HOTELS,
  payload,
});

/**
 * Action to fetch nearby hotels
 * @param {*} location
 */
export const fetchNearByHotels = (location) => {
  return async (dispatch) => {
    try {
      dispatch(handleLoading(true));
      const nearbyHotelRes = await fetchNearByHotelsRemote(location);
      dispatch(saveNearbyHotels(nearbyHotelRes));
      dispatch(handleLoading(false));
    } catch (err) {
      dispatch(handleError(true));
    }
  };
};
