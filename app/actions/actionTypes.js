export const HANDLE_NETWORK_CONNECTIVITY = 'HANDLE_NETWORK_CONNECTIVITY';
export const HANDLE_LOADING = 'HANDLE_LOADING';
export const HANDLE_ERROR = 'HANDLE_ERROR';
export const HANDLE_LOCATION_PERMISSION = 'HANDLE_LOCATION_PERMISSION';
export const SAVE_LOCATION = 'SAVE_LOCATION';
export const SAVE_NEARBY_HOTELS = 'SAVE_NEARBY_HOTELS';
