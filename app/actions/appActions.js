import {
  HANDLE_ERROR,
  HANDLE_LOADING,
  HANDLE_NETWORK_CONNECTIVITY,
  HANDLE_LOCATION_PERMISSION,
  SAVE_LOCATION,
} from '../actions/actionTypes';

export const handleNetworkConnectivity = (payload) => ({
  type: HANDLE_NETWORK_CONNECTIVITY,
  payload,
});

export const handleLoading = (payload) => ({
  type: HANDLE_LOADING,
  payload,
});

export const handleError = (payload) => ({
  type: HANDLE_ERROR,
  payload,
});

export const handleLocationPermission = (payload) => ({
  type: HANDLE_LOCATION_PERMISSION,
  payload,
});

export const saveLocation = (payload) => ({
  type: SAVE_LOCATION,
  payload,
});
