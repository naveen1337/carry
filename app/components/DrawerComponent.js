import React from 'react';
import {SafeAreaView, FlatList, Text} from 'react-native';
import {strings} from '../i18n/i18n';

const DrawerComponent = (props) => {
  const routes = [
    {
      id: '1',
      name: strings('menu.placeOrder'),
      value: 'HomeScreen',
      icon: 'home',
    },
    {
      id: '2',
      name: strings('menu.myOrders'),
      value: 'orderlist',
      icon: 'list',
    },
    {
      id: '3',
      name: strings('menu.about'),
      value: 'about',
      icon: 'book',
    },
    {
      id: '4',
      name: strings('menu.privacyPolicy'),
      value: 'privacypolicy',
      icon: 'document',
    },
    {
      id: '5',
      name: strings('menu.shippingPolicy'),
      value: 'shippingpolicy',
      icon: 'document',
    },
    {
      id: '6',
      name: strings('menu.refundPolicy'),
      value: 'refundpolicy',
      icon: 'document',
    },
    {
      id: '7',
      name: strings('menu.terms'),
      value: 'terms',
      icon: 'information-circle',
    },
    {
      id: '8',
      name: strings('menu.contactUs'),
      value: 'contact',
      icon: 'contacts',
    },
    {
      id: '9',
      name: strings('menu.rateUs'),
      value: 'rateUs',
      icon: 'star',
    },
  ];

  return (
    <SafeAreaView>
      <FlatList
        data={routes}
        renderItem={(item) => {
          return <Text>{item.item.name}</Text>;
        }}
        keyExtractor={(item) => item.id}
      />
    </SafeAreaView>
  );
};

export default DrawerComponent;
