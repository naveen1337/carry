import React from 'react';
import {View, StyleSheet, Dimensions} from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

const SampleLoadingComponent = () => {
  return (
    <SkeletonPlaceholder>
      <View style={styles.inputSection}>
        <View style={styles.addressInput}></View>
      </View>
      <View style={styles.categoriesList}>
        <View style={styles.categoryIcon}></View>
        <View style={styles.categoryIcon}></View>
        <View style={styles.categoryIcon}></View>
        <View style={styles.categoryIcon}></View>
        <View style={styles.categoryIcon}></View>
        <View style={styles.categoryIcon}></View>
      </View>
      <View style={styles.banner}></View>
      <View style={styles.storeList}>
        <View style={styles.store}>
          <View style={styles.storeImage}></View>
          <View style={styles.storeText}></View>
        </View>
        <View style={styles.store}>
          <View style={styles.storeImage}></View>
          <View style={styles.storeText}></View>
        </View>
      </View>
    </SkeletonPlaceholder>
  );
};

const styles = StyleSheet.create({
  inputSection: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  addressInput: {
    height: 50,
    borderRadius: 10,
    width: Dimensions.get('window').width - 30,
    margin: 10,
  },
  categoriesList: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    overflow: 'scroll',
    margin: 10,
  },
  categoryIcon: {
    height: 50,
    width: 50,
    borderRadius: 50,
  },
  banner: {
    alignSelf: 'center',
    margin: 10,
    height: 175,
    borderRadius: 10,
    width: Dimensions.get('window').width - 30,
  },
  storeList: {
    flexDirection: 'column',
    alignItems: 'center',
    overflow: 'scroll',
  },
  store: {
    flexDirection: 'row',
    margin: 5,
  },
  storeImage: {
    height: 100,
    width: 100,
    borderRadius: 10,
    justifyContent: 'space-between',
  },
  storeText: {
    marginTop: 16,
    marginLeft: 10,
    height: 70,
    borderRadius: 10,
    width: Dimensions.get('window').width - 150,
  },
});

export default SampleLoadingComponent;
