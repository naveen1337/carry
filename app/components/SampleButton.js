import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableNativeFeedback,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import AppColors from '../utils/appColors';

const SampleButton = (props) => {
  const buttonText = props.buttonText;

  let TouchablePlatformSpecific =
    Platform.OS === 'ios' ? TouchableOpacity : TouchableNativeFeedback;
  return (
    <TouchablePlatformSpecific
      background={TouchableNativeFeedback.Ripple(AppColors.rippleColor)}
      onPress={() => props.handleButtonClick()}>
      <View style={styles.defaultButton}>
        <Text style={styles.buttonText}>{buttonText}</Text>
      </View>
    </TouchablePlatformSpecific>
  );
};

const styles = StyleSheet.create({
  defaultButton: {
    height: 60,
    backgroundColor: AppColors.secondaryColor,
    width: Dimensions.get('window').width - 30,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    elevation: 5,
  },
  buttonText: {
    fontSize: 16,
    color: AppColors.whiteColor,
    textTransform: 'uppercase',
  },
});

export default SampleButton;
