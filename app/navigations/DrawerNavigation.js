import React from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import BottomTabNavigation from './BottomTabNavigation';
import DrawerComponent from '../components/DrawerComponent';

const Drawer = createDrawerNavigator();

const DrawerNavigation = () => {
  return (
    <Drawer.Navigator drawerContent={(props) => <DrawerComponent {...props} />}>
      <Drawer.Screen name="home" component={BottomTabNavigation} />
    </Drawer.Navigator>
  );
};

export default DrawerNavigation;
