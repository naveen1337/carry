import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import AddressSelectionScreen from '../screens/AddressSelectionScreen';
import AddressEntryScreen from '../screens/AddressEntryScreen';
import HomeScreen from '../screens/HomeScreen';
import AppColors from '../utils/appColors';
import TopSellerScreen from '../screens/TopSellerScreen';
import NearByResturantsScreen from '../screens/NearByResturantsScreen';
import ProductsScreen from '../screens/ProductsScreen';
import CartScreen from '../screens/CartScreen';
import OffersScreen from '../screens/OffersScreen';
import SignInScreen from '../screens/SignInScreen';
import SignUpScreen from '../screens/SignUpScreen';
import SearchScreen from '../screens/SearchScreen';
import ProfileScreen from '../screens/ProfileScreen';
import OrderHistoryScreen from '../screens/OrderHistoryScreen';
import OrderDetailsScreen from '../screens/OrderDetailsScreen';

const Stack = createStackNavigator();

const screenOptionsConfig = {
  headerTitleAlign: 'center',
  headerTitleStyle: {
    textTransform: 'uppercase',
    color: AppColors.whiteColor,
  },
  headerStyle: {
    backgroundColor: AppColors.primaryColor,
  },
};

export const AddressStack = (props) => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="AddressSelectionSreen"
        component={AddressSelectionScreen}
      />
      <Stack.Screen name="AddressEntryScreen" component={AddressEntryScreen} />
    </Stack.Navigator>
  );
};

export const HomeStack = (props) => {
  return (
    <Stack.Navigator
      initialRouteName={HomeScreen}
      screenOptions={screenOptionsConfig}>
      <Stack.Screen name="HomeScreen" component={HomeScreen} />
      <Stack.Screen name="TopSellerScreen" component={TopSellerScreen} />
      <Stack.Screen
        name="NearByResturantsScreen"
        component={NearByResturantsScreen}
      />
      <Stack.Screen name="ProductsScreen" component={ProductsScreen} />
      <Stack.Screen name="CartScreen" component={CartScreen} />
      <Stack.Screen name="OffersScreen" component={OffersScreen} />
      <Stack.Screen name="SignInScreen" component={SignInScreen} />
      <Stack.Screen name="SignUpScreen" component={SignUpScreen} />
    </Stack.Navigator>
  );
};

export const CartStack = (props) => {
  return (
    <Stack.Navigator
      initialRouteName={CartScreen}
      screenOptions={screenOptionsConfig}>
      <Stack.Screen name="CartScreen" component={CartScreen} />
      <Stack.Screen name="OffersScreen" component={OffersScreen} />
      <Stack.Screen name="SignInScreen" component={SignInScreen} />
      <Stack.Screen name="SignUpScreen" component={SignUpScreen} />
    </Stack.Navigator>
  );
};

export const SearchStack = (props) => {
  return (
    <Stack.Navigator
      initialRouteName={SearchScreen}
      screenOptions={screenOptionsConfig}>
      <Stack.Screen name="SearchScreen" component={SearchScreen} />
      <Stack.Screen name="ProductsScreen" component={ProductsScreen} />
      <Stack.Screen name="CartScreen" component={CartScreen} />
      <Stack.Screen name="OffersScreen" component={OffersScreen} />
      <Stack.Screen name="SignInScreen" component={SignInScreen} />
      <Stack.Screen name="SignUpScreen" component={SignUpScreen} />
    </Stack.Navigator>
  );
};

export const ProfileStack = (props) => {
  return (
    <Stack.Navigator
      initialRouteName={ProfileScreen}
      screenOptions={screenOptionsConfig}>
      <Stack.Screen name="ProfileScreen" component={ProfileScreen} />
      <Stack.Screen name="OrderHistoryScreen" component={OrderHistoryScreen} />
      <Stack.Screen name="OrderDetailsScreen" component={OrderDetailsScreen} />
      <Stack.Screen name="AddressEntryScreen" component={AddressEntryScreen} />
    </Stack.Navigator>
  );
};
