import React from 'react';
import {StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import AppColors from '../utils/appColors';
import {
  HomeStack,
  AddressStack,
  SearchStack,
  CartStack,
  ProfileStack,
} from './StackNavigation';
import {strings} from '../i18n/i18n';

const Tab = createMaterialBottomTabNavigator();

const BottomTabNavigation = ({navigation}) => {
  return (
    <Tab.Navigator
      barStyle={styles.barStyles}
      initialRouteName={HomeStack}
      activeColor={AppColors.secondaryColor}
      inactiveColor={AppColors.whiteColor}>
      <Tab.Screen
        name={strings('bottomTab.home')}
        component={HomeStack}
        options={{
          tabBarLabel: strings('bottomTab.home'),
          tabBarIcon: ({tintColor}) => (
            <Icon name="ios-home" color={AppColors.secondaryColor} size={25} />
          ),
        }}
      />
      <Tab.Screen
        name={strings('bottomTab.search')}
        component={SearchStack}
        options={{
          tabBarLabel: strings('bottomTab.search'),
          tabBarIcon: ({tintColor}) => (
            <Icon
              name="ios-search"
              color={AppColors.secondaryColor}
              size={25}
            />
          ),
        }}
      />
      <Tab.Screen
        name={strings('bottomTab.cart')}
        component={CartStack}
        options={{
          tabBarLabel: strings('bottomTab.cart'),
          tabBarIcon: ({tintColor}) => (
            <Icon name="ios-cart" color={AppColors.secondaryColor} size={25} />
          ),
        }}
      />
      <Tab.Screen
        name={strings('bottomTab.profile')}
        component={ProfileStack}
        options={{
          tabBarLabel: strings('bottomTab.profile'),
          tabBarIcon: ({tintColor}) => (
            <Icon
              name="ios-person"
              color={AppColors.secondaryColor}
              size={25}
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

const styles = StyleSheet.create({
  barStyles: {
    backgroundColor: AppColors.primaryColor,
  },
});

export default BottomTabNavigation;
