import {
  HANDLE_ERROR,
  HANDLE_LOADING,
  HANDLE_LOCATION_PERMISSION,
  HANDLE_NETWORK_CONNECTIVITY,
  SAVE_LOCATION,
} from '../actions/actionTypes';

const initialState = {
  isOnline: true,
  isError: false,
  isLoading: false,
  hasLocationPermission: null,
  location: {
    latitude: 10.3833,
    longitude: 78.8001,
  },
};

export const app = (state = initialState, action) => {
  switch (action.type) {
    case HANDLE_ERROR:
      return {
        ...state,
        isError: action.payload,
      };
    case HANDLE_LOADING:
      return {
        ...state,
        isLoading: action.payload,
      };
    case HANDLE_LOCATION_PERMISSION:
      return {
        ...state,
        hasLocationPermission: action.payload,
      };
    case HANDLE_NETWORK_CONNECTIVITY:
      return {
        ...state,
        isOnline: action.payload,
      };
    case SAVE_LOCATION:
      return {
        ...state,
        location: action.payload,
      };
    default:
      return state;
  }
};
