import {SAVE_NEARBY_HOTELS} from '../actions/actionTypes';

const initialState = {
  nearbyHotels: [],
  topSellerHotels: [],
};

export const home = (state = initialState, action) => {
  switch (action.type) {
    case SAVE_NEARBY_HOTELS:
      return {
        ...state,
        nearbyHotels: action.payload,
      };
    default:
      return state;
  }
};
