import {combineReducers} from 'redux';
import {app} from './appReducer';
import {home} from './homeReducer';
export default combineReducers({
  app,
  home,
});
