import * as _ from 'lodash';
import {handleError} from '../actions/appActions';
import store from '../store';
import {HTTP_METHODS} from './constants';

let svc_endpoint = 'http://tribata.in/dashboard/API/user/';
let svc_nearby_hotel = 'nearby_hotel.php';

/**
 * Method to fetch near by hotel by passing latitude and longitude
 * @param {*} location
 */
export const fetchNearByHotelsRemote = async (location) => {
  try {
    let svc_url = svc_endpoint + svc_nearby_hotel;
    let payload = location;
    const nearByHotelRes = await requestServer(
      HTTP_METHODS.POST,
      svc_url,
      payload,
    );
    return nearByHotelRes;
  } catch (err) {
    store.dispatch(handleError(true));
  }
};

/**
 * Generic method to make calls to the backend
 * @param {*} method
 * @param {*} url
 * @param {*} payload
 */
let requestServer = async (method, url, payload) => {
  let currentState = store.getState();
  return new Promise((resolve, reject) => {
    try {
      if (method == HTTP_METHODS.GET) {
        url =
          url +
          (_.includes(url, '?') ? '&' : '?') +
          _.chain(payload)
            .toPairs()
            .map(function (e) {
              return e.join('=');
            })
            .join('&')
            .value();
        payload = {};
      }
      if (method == HTTP_METHODS.GET || method == HTTP_METHODS.POST) {
        let options = {
          method: method,
          headers: {
            'Content-Type': 'application/json',
          },
        };
        if (method == HTTP_METHODS.POST) {
          options.body = JSON.stringify(payload);
        }
        fetch(url, options).then((serverResponse) => {
          if (serverResponse.ok) {
            serverResponse.json().then((data) => {
              resolve(data);
            });
          } else {
            serverResponse.json().then((data) => {
              reject(data);
            });
          }
        });
      }
    } catch (err) {
      throw err;
    }
  });
};
