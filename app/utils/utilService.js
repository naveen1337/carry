import {PermissionsAndroid} from 'react-native';
import firebase from 'react-native-firebase';
import AsyncStorage from '@react-native-community/async-storage';
import NetInfo from '@react-native-community/netinfo';
import SInfo from 'react-native-sensitive-info';
import Geolocation from 'react-native-geolocation-service';
import Geocoder from 'react-native-geocoding';
import store from '../store';
import {
  handleLocationPermission,
  handleNetworkConnectivity,
  saveLocation,
} from '../actions/appActions';

/**
 * Method to get token from shared preference and key chain
 */
export const getToken = async (tokenName) => {
  try {
    const token = await SInfo.getItem(tokenName, {
      sharedPreferencesName: 'tribata_shared_prefs',
      keychainService: 'tribata_keychain',
    });
    return token;
  } catch (err) {
    store.dispatch(handleError(true));
  }
};

/**
 * Method to delete token from shared preference and key chain
 */
export const deleteToken = async (tokenName) => {
  try {
    const delToken = await SInfo.deleteItem(tokenName, {
      sharedPreferencesName: 'tribata_shared_prefs',
      keychainService: 'tribata_keychain',
    });
    return delToken;
  } catch (err) {
    store.dispatch(handleError(true));
  }
};

/**
 * Method to set token in key chain
 */
export const setToken = async (tokenName, token) => {
  try {
    const tokenRes = await SInfo.setItem(tokenName, token, {
      sharedPreferencesName: 'tribata_shared_prefs',
      keychainService: 'tribata_keychain',
    });
  } catch (err) {
    store.dispatch(handleError(true));
  }
};

/**
 * Method to determine network connectivity
 */
export const getNetworkState = async () => {
  try {
    const networkState = await NetInfo.fetch();
    if (networkState && networkState.type) {
      if (networkState.type !== 'none' && networkState.type !== 'unknown') {
        if (networkState.isInternetReachable) {
          store.dispatch(handleNetworkConnectivity(true));
        } else {
          store.dispatch(handleNetworkConnectivity(false));
        }
      } else {
        store.dispatch(handleNetworkConnectivity(false));
      }
    }
  } catch (err) {
    store.dispatch(handleNetworkConnectivity(false));
  }
};

/**
 * Method to request location permission
 */
export const requestLocationPermission = async () => {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      store.dispatch(handleLocationPermission(true));
    } else {
      store.dispatch(handleLocationPermission(false));
    }
  } catch (err) {
    store.dispatch(handleLocationPermission(false));
  }
};

/**
 * Method to get location coordinates
 */
export const getLocationCoordinates = () => {
  return new Promise((resolve, reject) => {
    let currentState = store.getState();
    if (currentState.app.hasLocationPermission) {
      Geolocation.getCurrentPosition(
        (position) => {
          getAddressFromLocation(
            position.coords.latitude,
            position.coords.longitude,
          ).then((res) => {
            store.dispatch(
              saveLocation({
                latitude: position.coords.latitude,
                longitude: position.coords.longitude,
                address: res,
              }),
            );
            resolve();
          });
        },
        (err) => {
          saveLocation({
            latitude: null,
            longitude: null,
          }),
            reject(err);
        },
        {
          enableHighAccuracy: true,
          timeout: 25000,
          maximumAge: 3600000,
        },
      );
    }
  });
};

/**
 * Method to do reverse geocoding
 */
export const getAddressFromLocation = (lat, lng) => {
  return new Promise((resolve, reject) => {
    Geocoder.from(lat, lng)
      .then((res) => {
        console.log(res);
        let address = res.results[0].formatted_address;
        resolve(address);
      })
      .catch((error) => {
        console.log(error);
        reject(error);
      });
  });
};

/**
 * Method to get fcm token
 */
export const storeDeviceToken = async () => {
  try {
    const fcmToken = await firebase.messaging().getToken();
    if (fcmToken) {
      console.log('fcm token', fcmToken);
      await AsyncStorage.setItem('DEVICEID', fcmToken);
    } else {
      // todo catch error - decide what to do...
      console.log('Unable to fetch device token');
    }
    return fcmToken;
  } catch (err) {
    // todo catch error - decide what to do...
    throw err;
  }
};
