import AppColors from './appColors';

const AppStyles = {
  buttonText: {
    fontSize: 16,
    color: AppColors.primaryColor,
    textTransform: 'uppercase',
  },
};

export default AppStyles;
