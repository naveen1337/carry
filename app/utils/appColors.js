const AppColors = {
  primaryColor: '#000000',
  secondaryColor: '#CC9C4E',
  whiteColor: '#FFFFFF',
  rippleColor: '#D8D8D8',
};

export default AppColors;
